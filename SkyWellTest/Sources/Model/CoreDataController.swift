//
//  CoreDataController.swift
//  SkyWellTest
//
//  Created by Bogdan Kalashnikov on 10/24/16.
//  Copyright © 2016 Bogdan Kalashnikov. All rights reserved.
//

import CoreData

class CoreDataController: NSObject {
	
	// MARK: - Core Data stack
	
	lazy var persistentContainer: NSPersistentContainer = {
		let container = NSPersistentContainer(name: "SkyWellTest")
		container.loadPersistentStores(completionHandler: { (storeDescription, error) in
			if let error = error as NSError? {
				print(error)
			}
		})
		return container
	}()
	
	// MARK: - Core Data Saving support
	
	func saveContext () {
		let context = persistentContainer.viewContext
		
		if context.hasChanges {
			do {
				try context.save()
			} catch {
				print(error)
			}
		}
	}

}
