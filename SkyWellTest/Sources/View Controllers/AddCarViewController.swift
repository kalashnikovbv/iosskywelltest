//
//  AddCarViewController.swift
//  SkyWellTest
//
//  Created by Bogdan Kalashnikov on 10/24/16.
//  Copyright © 2016 Bogdan Kalashnikov. All rights reserved.
//

import UIKit

let kShowPickerViewSegueIdentifier = "ShowPickerViewSegue"

class AddCarViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }    

    // MARK: - Navigation
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let identifier = segue.identifier {
			if identifier == kShowPickerViewSegueIdentifier {
				if let controller = segue.destination as? PickerViewController {
					controller.options = ["Automatic", "Manual", "Robotic"]
					controller.currentOption = 1
					controller.delegate = self
				}
			}
		}
    }
	
}

extension AddCarViewController: PickerViewControllerDelegate {
	
	func pickerViewController(_ controller: PickerViewController, didSelectOption index: Int) {
		print("Selected option: \(index)")
	}
	
}
