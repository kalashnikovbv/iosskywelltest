//
//  PickerViewController.swift
//  SkyWellTest
//
//  Created by Bogdan Kalashnikov on 10/24/16.
//  Copyright © 2016 Bogdan Kalashnikov. All rights reserved.
//

import UIKit

protocol PickerViewControllerDelegate: class {
	func pickerViewController(_ controller: PickerViewController, didSelectOption index:  Int)
}

class PickerViewController: UIViewController {
	
	var options: [String] = []
	var currentOption: Int = 0
	
	weak var delegate: PickerViewControllerDelegate?
	
	// MARK: - IBOutlets
	
	@IBOutlet weak var pickerView: UIPickerView!
	
	// MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		pickerView.selectRow(currentOption, inComponent: 0, animated: animated)
	}
	
	// MARK: - IBActions
	
	@IBAction func confirm(_ sender: UIButton) {
		dismiss(animated: true, completion: nil)
		delegate?.pickerViewController(self, didSelectOption: pickerView.selectedRow(inComponent: 0))
	}
	
	@IBAction func dismiss(_ sender: UIButton) {
		dismiss(animated: true, completion: nil)
	}

}

extension PickerViewController: UIPickerViewDataSource {
	
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return options.count
	}
	
}

extension  PickerViewController: UIPickerViewDelegate {
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		return options[row]
	}
	
}
