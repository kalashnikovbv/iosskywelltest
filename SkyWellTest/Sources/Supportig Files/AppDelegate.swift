//
//  AppDelegate.swift
//  SkyWellTest
//
//  Created by Bogdan Kalashnikov on 10/24/16.
//  Copyright © 2016 Bogdan Kalashnikov. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	
	var coreDataController = CoreDataController()
	
	static func sharedAppDelegate() -> AppDelegate? {
		return UIApplication.shared.delegate as? AppDelegate
	}
	
	// MARK: - UIApplicationDelegate

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		return true
	}

	func applicationWillResignActive(_ application: UIApplication) {
		coreDataController.saveContext()
	}

}

